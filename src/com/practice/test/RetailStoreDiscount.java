package com.practice.test;

import java.util.Scanner;

/**
 * This is RetailStoreDiscount class which contains static method like
 * nearestDownValue, discountOnBill and calculateBill to calculate the payable
 * amount accordingly various discounts like role base or transactions base.
 * 
 * @author manishk
 * @version 1.0 - 01 Dec, 2019
 */
public class RetailStoreDiscount {

	private static final int USER_EMP = 1;
	private static final int USER_AFFILIATE = 2;
	private static final int USER_LONG_TERM = 3;
	private static final int GROCERIES_ITEM = 1;
	private static final int TRANS_BASE_DISCOUNT = 5;
	private static final int USER_EMP_DISCOUNT = 30;
	private static final int USER_AFFILIATE_DISCOUNT = 10;
	private static final int USER_LONG_TERM_DISCOUNT = 5;
	private static final int HUNDRED = 100;
	private static Scanner readInput = null;

	/**
	 * Method that contains the logic to find the nearest down value for example 235
	 * gives 200 and 375 gives 300 and so on.
	 */
	public static double nearestDownValue(double bill) {
		return bill - (bill % HUNDRED);
	}

	/**
	 * Method that contains the logic to calculate discount on the basis of user
	 * role like employee as customer and customer affiliated by store etc.
	 */
	public static double discountOnBill(double discount, double bill) {
		double roleBaseDiscount = (bill * discount) / HUNDRED;
		System.out.println("\t Role based discount on total bill : " + roleBaseDiscount);
		return roleBaseDiscount;
	}

	/**
	 * Method that contains the logic to find the Net payable amount on the basises
	 * of role base discount and transaction base discount.
	 */
	public static double calculateBill(double totalBill) {

		if (totalBill >= HUNDRED) {
			double transDiscount = (nearestDownValue(totalBill) * TRANS_BASE_DISCOUNT) / 100;
			System.out.println("\t Transaction Discount on total bill : " + transDiscount);
			totalBill = totalBill - transDiscount;
		}
		return totalBill;
	}

	/**
	 * Method that contains the logic to assign the discount on 
	 * the basis of user role.
	 */
	public static double discountOnRolebase() {

		double roleBaseDiscount = 0.0;
		int userRole = 0;

		System.out.println("\t Please enter the role of User :\n");
		System.out.println("\t Press <1> for user as Employee.");
		System.out.println("\t Press <2> user is an Affiliate of the store.");
		System.out.println("\t Press <3> user is customer for over 2 years.\n\n");

		userRole = readInput.nextInt();
		System.out.println("\t Entered : <User Role> is :" + userRole + "\n");
		
		switch (userRole) {

		case USER_EMP:
			roleBaseDiscount = USER_EMP_DISCOUNT;
			break;

		case USER_AFFILIATE:
			roleBaseDiscount = USER_AFFILIATE_DISCOUNT;
			break;

		case USER_LONG_TERM:
			roleBaseDiscount = USER_LONG_TERM_DISCOUNT;
			break;

		}
		return roleBaseDiscount;
	}

	/**
	 * This method is entry point where application is execute and it takes input
	 * parameter to run application such as role of user, item type.
	 */

	public static void main(String[] args) {

		double roleBaseDiscount = 0.0, totalBill = 0.0, billAmountAfterDiscount = 0.0;
		int itemType = 0;
		// Scanner is used to take input from user
		readInput = new Scanner(System.in);

		System.out.println("\t Please enter the Item Type\n");
		System.out.println("\t Press <1> for Groceries Item.");
		System.out.println("\t Press <2> for Non-Groceries Item.");
		itemType = readInput.nextInt();

		System.out.println("\t Enter total bill amount.");
		totalBill = readInput.nextInt();
		System.out.println("\t Entered : <Item type> is  :" + itemType +"\n");

		/*
		 * Controls goes to if block if item is groceries type if not then goes to else
		 * block and ask for user role and gives discount accordingly.
		 */
		if (GROCERIES_ITEM == itemType)
			roleBaseDiscount = 0.0;
		else
			roleBaseDiscount = discountOnRolebase();


		if (roleBaseDiscount > 0)
			billAmountAfterDiscount = totalBill - discountOnBill(roleBaseDiscount, totalBill);
		else {
			billAmountAfterDiscount = totalBill;
			System.out.println("\t Role based discount on total bill : " + roleBaseDiscount);
		}
		System.out.println("\n\t Net Payable Amount : " + calculateBill(billAmountAfterDiscount));
	}
}
